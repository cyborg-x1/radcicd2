#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd $SCRIPT_DIR
ansible-playbook ./ansible/bringup-minikube.ap.yml -K $*
cd -
