alias kexec='kubectl exec -it'
alias minikube_docker='eval $(minikube -p minikube docker-env)'

function kustomize {
    docker run --rm -it -v $(pwd):/cwd k8s.gcr.io/kustomize/kustomize:v3.8.7 build /cwd/$1
}
